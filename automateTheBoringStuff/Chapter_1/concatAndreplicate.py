# Operators can perform different functions depending on the data type that's being operated on
# For example, '+' will add 2 integers or float numbers together but when used with strings on both sides....it will concatenate both of the strings together

var1 = 'String'
var2 = 'NextString'

var3 = (var1 + var2)
print (var3)

# Python can't concat 2 different data types. If you tried a "String1" + 42..it will throw an error.
# A way to get around this is called "casting". Say you have a string value of 42 but you want to do math on it...just declare the type int("42") 


# * performs multiplication on 2 integers / floats. But when used with strings, it'll just replicate the string as many times as you specify

var4 = 'String'
var5 = (var4 * 5)
print (var5)