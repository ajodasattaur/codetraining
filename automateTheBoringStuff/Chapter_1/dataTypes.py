# Three common data types in python are strings, integers (int), floating point

# Strings are just plain text. Not evaluated down to any ValueError
# Integers are whole numbers, without a decimal
# Floating point numbers are numbers with decimals

# Strings should be surrounded by single quotes
var1 = 'This is a string data type'
print (var1)

# Integers
var2 = 42
print (var2)

# Floating points
var3 = 42.0
print (var3)