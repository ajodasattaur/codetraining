# Expressions consist of values and operators
# They can always evaluate (reduce) to a single value
# 2 + 2 evaluates to 4
# Single value with no operators is also an expression but it only evaluates to itself
# i.e 2 evaluates to 2 
2 + 2 