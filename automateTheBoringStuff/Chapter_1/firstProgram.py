# Program to read user input and then echo it out 

print ('Hello world!')
print ('What is your name: ')  # prompting user for input
myName = raw_input()  # store user input after print message

print ('Its good to meet you, ' + myName)
print ('The length of your name is: ')
print (len(myName))

myAge = raw_input('How old are you?: \n') # storing user input as variable in a single line


# The value of myAge is a string when its initialized. We then call int() function to treat the value as an integer to do some math. Then that value is passed into the str() function so that it can 
# be concatenated back into the final string

print ('You will be ' + str(int(myAge) + 1 ) + ' in a year')



# The basics of how the above line is evaluated is below:
# We substitue the value of myAge into myAge so the expression then becomes str(int(25) + 1)

# The value is inserted as a string here so the expression then becomes str(int('25') + 1)
# You can't do math operations on a str so we pass the value into the int() function so it becomes str(int(25) +1)

# Then we move on to the brackets and start doing the math operations we want so the expression becomes str(25 + 1), which evaluates to str(26)

# You can't concat a str and an int so we then pass the final evaluated value into the str() function. 
# This means the line becomes print ('You will be ' + str('26') + ' in a year')

# Finally all three strings are concatenaed together so the value becomes print ('You will be 26 in a year')