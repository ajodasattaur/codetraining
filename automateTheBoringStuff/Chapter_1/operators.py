# There are 7 math operators in python
# Order of operations matches real life math operations
# First **, then *,/,//,% and then + and - are evaluated last

# ** = exponent
var1 = 2 ** 2
print ("2 ** 2 evaulates to", var1)

# % modulo evaluates to the remained of an expression
var2 = 23  % 7
print (var2)

# Division
var3 = 22 // 8
print (var3)