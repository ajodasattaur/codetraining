# Variables are initialized the first time that a value is stored in it
# You can assign values with the '=' operator
# You can also reassign the value of a variable depending on where you are in your script
# 

var1 = 4 + 4
print (var1)

var1 = 8 + 8
print (var1)