# Three different operators can compare Boolean values
# and, or, not

# And evaluates to True if both Boolean values are True, otherwise it's False

True and True # Try running this in the interpreter shell. It should evaluate to True. Both sides are true

True and False # This would evaluate to False since one side of the expression is False

# Or evaluates to true if either side of the expression is True

True or False # Should return True
False or False # Should return False

# Not operates on only one Boolean value. Evaluates to the opposite of the Boolean value

not True # should evaluate to False
not not True # nested operator. Never a real reason to do this in the real world. Should evaluate to True