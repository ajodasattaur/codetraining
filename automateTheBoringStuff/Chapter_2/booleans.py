# Boolean's have only 2 possible values in python...True or False
# You can set variables to boolean values but they always need to be capitalized
# Boolean's are always unquoted

VAR_1 = True
print VAR_1
