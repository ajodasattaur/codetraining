# Comparison operators compare 2 values against each other and evaluate down to a single Boolean value 
# i.e True or False


42 == 42 # using the equal to operator to compare integer values
         # The above code will evaluate down to True since both integers are equal