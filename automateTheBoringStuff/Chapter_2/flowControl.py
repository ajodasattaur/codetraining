# You can control the operations taken in your python code by using control methods such as if else statements
# This script will take certain actions if a variable is equal to a certain string

name = 'Amar' # Variable assignment
password = 'Test'

if name == 'Amar': # flow control block start. Only takes action if the name variable is equal to the string 'Amar'
    print('Your name is Amar')
    if password == 'Test': # second flow control block. Only runs if password var is equal to string 'Test'
        print('Access Granted')
    else: # Exception handling
        print('Access not granted')
    