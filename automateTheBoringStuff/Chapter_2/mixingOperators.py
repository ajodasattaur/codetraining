# You can mix Boolean and comparison operators to perform different tasks

#####################################################
# Expressions in this section should evaluate to True

(4 < 5) and (5 > 1) # Run this in the shell and test the return value
                    # This is just a plain expression on the shell. Now try variables

var1 = 5
var2 = 34
var3 = 4554
var4 = 232323

(var1 < var2) and (var4 > var3) 


