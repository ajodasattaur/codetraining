# While loops will execute a block of code as long as the condition of the statement is True
# I've used this before at Pattison without knowing what it really was
# I used an infinite loop...that is to say a while loop when the value is ALWAYS True
# while true; do thing; sleep 1; done   There's nowhere in the code that while is False so it runs forever

# Let's run a test block without while

VAR_1 = 0
if VAR_1 < 5:
    print ('Hello world')
    VAR_1 = VAR_1 + 1
# This code will execute only once because I never loop over the block after VAR_1 is increased

VAR_2 = 0
while VAR_2 < 2: # perform the action only while var 2 is less than x
    print ('Hello world. Im Amar') # Action to execute
    VAR_2 = VAR_2 + 1 # Increase variable value by once each time the loop runs
