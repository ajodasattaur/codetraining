# the python interpreter shell will do math operations for you
# the operators are 
# "+" for addtion
# "-" for subtraction
# "/" for division
# "*" for multiplication
# "**" for exponents
# "%" for remainders

addVar = 10 + 10
subVar = 10 - 5
multVar = 5 * 5
divVar = 10 / 5

print addVar
print subVar
print multVar
print divVar