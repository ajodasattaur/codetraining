# You can reassign the value of a varible depending on where it is in a script

first_var = 3
print first_var # first_var is equal to 3 at this point so that's what will be printed

first_var = 10 # Now we've changed the value to 10 so if we print it'll show 10
print first_var # This is whats known as "reassignment"
